﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DestroyOnYDown : MonoBehaviour {

    bool isRed;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if ((Input.GetKeyDown(KeyCode.Y) && isRed)) {
            isRed = false;
            gameObject.GetComponent<Image>().color = Color.green;
        }
        else if ((Input.GetKeyDown(KeyCode.Y) && !isRed)) {
            isRed = true;
            gameObject.GetComponent<Image>().color = Color.red;
        }
    }
}
